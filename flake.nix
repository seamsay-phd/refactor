{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};

      tex = pkgs.texlive.combine {
        inherit (pkgs.texlive) scheme-basic biblatex bibtex cleveref csquotes ieeetran minted newfloat xetex;
        # Needed for LaTeX Workshop.
        inherit (pkgs.texlive) chktex latexindent;
      };
      python = pkgs.python3.withPackages (ps: with ps; [pygments]);

      src = ./.;

      installFlags = ["PUBLIC=$(out)"];
    in {
      packages = {
        default = self.packages.${system}.pdf;

        pdf = pkgs.stdenv.mkDerivation {
          pname = "Refactor-Report-PDF";
          version = "0.0.0";

          inherit src installFlags;

          # NOTE: `minted` requires `which`.
          buildInputs = with pkgs; [biber d2 librsvg python rubber tex which];
        };
      };

      devShells.default = pkgs.mkShell {
        inputsFrom = builtins.attrValues self.packages.${system};

        inherit src installFlags;
      };

      formatter = pkgs.alejandra;
    });
}
