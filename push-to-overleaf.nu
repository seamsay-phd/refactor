#!/usr/bin/env nu

def copy-relative [file: path, relative: path, to: path = .] {
    let relpath = ($file | path relative-to $relative)
    let newpath = ($to | path join $relpath)

    mkdir ($newpath | path dirname)
    cp $file $newpath
}

def copy-all-relative [pattern: glob, relative: path, to: path = .] {
    ls $pattern | get name | path expand | each { |file|
        copy-relative $file $relative $to
    }
}

if not (echo push-to-overleaf.nu | path exists) {
    error make {msg: "Script assumes you're in its directory."}
}

let current_branch = (git rev-parse --abbrev-ref HEAD)

git switch main

make

git switch overleaf
nu pull-from-overleaf.nu
git merge --no-edit main

cp build/Refactor.tex .
cp build/Refactor.bib .

copy-all-relative build/Diagrams/**/*.jpg build
copy-all-relative build/Diagrams/**/*.pdf build

git add Refactor.tex Refactor.bib Diagrams/
do --ignore-program-errors { git commit -m 'AUTO: Commit Overleaf files.' }

git push overleaf overleaf:master

git switch $current_branch