#!/usr/bin/env nu

if not (echo pull-from-overleaf.nu | path exists) {
    error make {msg: "Script assumes you're in its directory."}
}

let current_branch = (git rev-parse --abbrev-ref HEAD)

git switch overleaf
git pull --rebase=false --no-edit overleaf master

cp Refactor.tex src/Refactor.tex

git add src/Refactor.tex
do --ignore-program-errors { git commit -m 'AUTO: Commit file changed on Overleaf.' }

git switch $current_branch