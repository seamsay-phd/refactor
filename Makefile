PUBLIC ?= public

D2 ?= d2
RUBBER ?= rubber

D2_FLAGS ?= --layout elk

SRC := src
BUILD := build
NAME := Refactor

D2_INPUTS := $(shell find $(SRC) -name '*.d2')
D2_SVG_OUTPUTS := $(patsubst $(SRC)/%.d2,$(BUILD)/%.svg,$(D2_INPUTS))
D2_PDF_OUTPUTS := $(patsubst %.svg,%.pdf,$(D2_SVG_OUTPUTS))

COPY_FILES := $(SRC)/Diagrams/vicious.jpg $(SRC)/$(NAME).bib
COPY_FILE_OUTPUTS := $(patsubst $(SRC)/%,$(BUILD)/%,$(COPY_FILES))

.PHONY: all
all: pdf

.PHONY: d2
d2: $(D2_SVG_OUTPUTS)

.PHONY: pdf
pdf: $(BUILD)/$(NAME).pdf

.PHONY: install
install: $(BUILD)/$(NAME).pdf
	install -d $(PUBLIC)
	install $(BUILD)/$(NAME).pdf $(PUBLIC)

$(D2_SVG_OUTPUTS): $(BUILD)/%.svg: $(SRC)/%.d2
	mkdir -p $(BUILD)
	d2 $(D2_FLAGS) $< $@

$(D2_PDF_OUTPUTS): %.pdf: %.svg
	rsvg-convert --format pdf --output $@ $<

$(COPY_FILE_OUTPUTS): $(BUILD)/%: $(SRC)/%
	install -d $(BUILD)
	install $< $@

$(BUILD)/$(NAME).tex: $(SRC)/$(NAME).tex
	install -d $(BUILD)
	install $< $@

$(BUILD)/$(NAME).pdf: $(BUILD)/$(NAME).tex $(D2_PDF_OUTPUTS) $(COPY_FILE_OUTPUTS)
	$(RUBBER) --into $(BUILD) --unsafe --module xelatex $(RUBBER_FLAGS) $<

.PHONY: clean
clean:
	$(RM) -r $(BUILD) $(PUBLIC) result
